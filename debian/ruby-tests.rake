require 'gem2deb/rake/spectask'

fakehome = File.dirname(__FILE__) + "/fakehome"
Dir.mkdir(fakehome) unless File.exist?(fakehome)
ENV["HOME"] = fakehome

Gem2Deb::Rake::RSpecTask.new do |spec|
  spec.pattern = './spec/**/*_spec.rb'
  spec.exclude_pattern = './spec/acceptance/*'
end
